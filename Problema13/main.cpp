#include <iostream>

using namespace std;

int valorPorPosicion(int fila, int columna, int **matriz){
    int valorEstrellaNominador=0, valorEstrella=0;

        //Se usa la ecuacion dada sumando los valores allegados a cada posicion
        valorEstrellaNominador = ((*(*(matriz+fila)+columna)) + (*(*(matriz+fila)+(columna-1))) + (*(*(matriz+fila)+(columna+1))) + (*(*(matriz+(fila-1))+columna)) + (*(*(matriz+(fila+1))+columna)) );
        valorEstrella = (valorEstrellaNominador/5);

    //Y retorna el valor resultante de la ecuacion
    return valorEstrella;
}

int main()
{
    /* Este programa toma una matriz de orden 6x8, que al recorrerla verificara por medio de una ecuacion si en la posicion que se encontrara el puntero
    se encuentra una estrella mediante al resultado de la ecuacion anterior, contandose el total de estrellas que se encontrara en la matriz*/

    //Variable temp que contiene los datos ya conocidos
    int temp[6][8]={{0,3,4,0,0,0,6,8},{5,13,6,0,0,0,2,3},{2,6,2,7,3,0,10,0},{0,0,4,15,4,1,6,0},{0,0,7,12,6,9,10,4},{5,0,6,10,6,4,8,0}};

    int **arreglo;                      //Se crea arreglo de dos dimensiones
    arreglo= new int*[6];               //Se divide el arreglo en 6 que contendra punteros
    for (int i=0;i < 6; i++){           //Se recorre cada una de las 6 divisiones anteriores
        arreglo[i]= new int[8];         //En cada una de las divisiones se crea divisiones de 8 que ya contendran datos (por ende no tiene *)
        for (int j=0;j<8;j++){          //Se recorre cada una de las 8 divisiones anteriores
            arreglo[i][j]=temp[i][j];   //Se le asigna a CADA valor de arreglo, el valor de la MISMA posicion de temp
        }
    }

    int valorEstrellaTotal=0, cantidadEstrellas=0;

    for (int i=1; i<5;i++){             //Contador de Filas
        for (int j= 1; j < 7; j++){     //Contador de Columnas

            valorEstrellaTotal=valorPorPosicion(i,j,arreglo);

                if (valorEstrellaTotal > 6){
                    cantidadEstrellas++;            //Si el resultado de la ecuacion dada es mayor que 6, significa que en ese espacio se encuentra una estrella
                }
            }
        }
    cout <<"La cantidad de estrellas que tiene la foto es de: "<<cantidadEstrellas<<endl;
    return 0;
}
